  $( function($) {
 
	$.Autocomplete=function(availableTags) {
    $( "#seachWord" ).autocomplete({
      source: availableTags
    });
  } });
  
  Tapestry.Initializer.textboxHint = function(spec) {
	    new Autocomplete(spec.availableTags);
	}