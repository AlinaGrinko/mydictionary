(function() {
  define(["jquery", "./events", "./dom", "bootstrap/modal"], function($, events, dom) {
    	
    	  $( function(availableTags) {
    		  
    		    $( "#searchedWord" ).autocomplete({
    		    	
    		      source: availableTags
    		      
    		    });
    	} );
  });
})